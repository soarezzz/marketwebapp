from flask import Flask

import csv
import json

app = Flask(__name__)

with open('clean-quote-api-master/quotes.csv') as f:
    reader = csv.DictReader(f)
    rows = list(reader)

with open('quotes.json', 'w') as f:
    json.dump(rows, f)

@app.route('/')
def hello_world():
    return 'Bienvenue dans MarketWebApp!'


if __name__ == '__main__':
    app.run(debug=True)
